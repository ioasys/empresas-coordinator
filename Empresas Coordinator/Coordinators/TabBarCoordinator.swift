//
//  TabBarCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol TabBarCoordinatorDelegate: CoordinatorDelegate {
    func didAskForLogout(on coordinator: TabBarCoordinator)
}

class TabBarCoordinator: NSObject, Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    var delegate: TabBarCoordinatorDelegate?
    var user: User
    private var tabBarController: UITabBarController?
    
    init(user: User, navigationController: UINavigationController, delegate: TabBarCoordinatorDelegate) {
        self.user = user
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
        self.delegate = delegate
    }
    
    func start() {
        
        self.childCoordinators = [self.createHomeCoordinator(), self.createFavoritesCoordinator(), self.createProfileCoordinator()]
        
        let tabBarController = BaseTabBarController()
        tabBarController.viewControllers = self.childCoordinators.map { $0.navigationController }
        tabBarController.delegate = self
        self.navigationController.setViewControllers([tabBarController], animated: false)
        self.navigationController.setNavigationBarHidden(true, animated: false)
        
        self.tabBarController = tabBarController
    }
    
    func createHomeCoordinator() -> HomeCoordinator {
        
        let navigationController = BaseNavigationController()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 0)
        
        let coordinator = HomeCoordinator(navigationController: navigationController, delegate: self)
        coordinator.start()
        return coordinator
    }
    
    func createFavoritesCoordinator() -> FavoritesCoordinator {
        
        let navigationController = BaseNavigationController()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
        
        let coordinator = FavoritesCoordinator(navigationController: navigationController, delegate: self)
        coordinator.start()
        return coordinator
    }
    
    func createProfileCoordinator() -> ProfileCoordinator {
        
        let navigationController = BaseNavigationController()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 0)
        
        let coordinator = ProfileCoordinator(user: self.user, navigationController: navigationController, delegate: self)
        coordinator.start()
        return coordinator
    }
    
    func show(company: Company) {
        guard let navigationController = self.tabBarController?.selectedViewController as? UINavigationController else { return }
        
        let coordinator = CompanyCoordinator(company: company, navigationController: navigationController, delegate: self)
        coordinator.start()
        
        self.childCoordinators.append(coordinator)
    }
}

// MARK: - Profile Coordinator delegate

extension TabBarCoordinator: ProfileCoordinatorDelegate {
    
    func didAskForLogout(on coordinator: ProfileCoordinator) {
        self.delegate?.didAskForLogout(on: self)
    }
}

// MARK: - Tab Bar Controller delegate

extension TabBarCoordinator: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if viewController == self.childCoordinators[1].navigationController {
            let favoritesCoordinator = self.childCoordinators[1] as! FavoritesCoordinator
            favoritesCoordinator.reload()
        }
    }
}
