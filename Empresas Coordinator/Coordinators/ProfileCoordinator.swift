//
//  ProfileCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol ProfileCoordinatorDelegate: CoordinatorDelegate {
    func didAskForLogout(on coordinator: ProfileCoordinator)
}

class ProfileCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    var delegate: ProfileCoordinatorDelegate?
    var user: User
    
    init(user: User, navigationController: UINavigationController, delegate: ProfileCoordinatorDelegate) {
        self.user = user
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
        self.delegate = delegate
    }
    
    func start() {
        self.showProfile()
    }
    
    func showProfile() {
        
        let profileViewController = ProfileViewController.initFromStoryboard(named: "Main")
        profileViewController.user = self.user
        profileViewController.delegate = self
        self.navigationController.pushViewController(profileViewController, animated: true)
    }
}

// MARK: - Profile View Controller delegate

extension ProfileCoordinator: ProfileViewControllerDelegate {
    
    func didTapLogout(on viewController: ProfileViewController) {
        self.delegate?.didAskForLogout(on: self)
    }
}
