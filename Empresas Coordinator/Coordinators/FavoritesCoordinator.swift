//
//  FavoritesCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class FavoritesCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    
    private var favoritesViewController: CompanyListViewController?
    
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        self.showFavorites()
        self.reload()
    }
    
    func reload() {
        
        CompanyAPI.favorites { response, error, cache in
            if let response = response {
                self.favoritesViewController?.companies = response
            }
        }
    }
    
    func showFavorites() {
        
        let favoritesViewController = CompanyListViewController.initFromStoryboard(named: "Main")
        favoritesViewController.title = NSLocalizedString("Favoritos", comment: "")
        favoritesViewController.delegate = self
        self.navigationController.pushViewController(favoritesViewController, animated: true)
        
        self.favoritesViewController = favoritesViewController
    }
    
    func show(company: Company) {
        
        let coordinator = CompanyCoordinator(company: company, navigationController: self.navigationController, delegate: self)
        coordinator.start()
        
        self.childCoordinators.append(coordinator)
    }
}

// MARK: - Coordinator delegate

extension FavoritesCoordinator: CoordinatorDelegate {
    
    func coordinatorDidExit(_ coordinator: Coordinator) {
        
        if coordinator is CompanyCoordinator {
            self.reload()
        }
    }
}

// MARK: - Company List View Controller delegate

extension FavoritesCoordinator: CompanyListViewControllerDelegate {
    
    func didSelect(company: Company, on viewController: CompanyListViewController) {
        self.show(company: company)
    }
}

