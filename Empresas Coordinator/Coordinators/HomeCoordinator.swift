//
//  HomeCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class HomeCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        self.showHome()
    }
    
    func showHome() {
        
        let homeViewController = HomeViewController.initFromStoryboard(named: "Main")
        homeViewController.delegate = self
        self.navigationController.pushViewController(homeViewController, animated: true)
    }
    
    func show(company: Company) {
        
        let coordinator = CompanyCoordinator(company: company, navigationController: self.navigationController, delegate: self)
        coordinator.start()
        
        self.childCoordinators.append(coordinator)
    }
}

// MARK: - Home View Controller delegate

extension HomeCoordinator: HomeViewControllerDelegate {
    
    func didSelect(company: Company, on viewController: HomeViewController) {
        self.show(company: company)
    }
}
