//
//  CompanyCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class CompanyCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    var company: Company
    
    init(company: Company, navigationController: UINavigationController, delegate: CoordinatorDelegate) {
        self.company = company
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        self.showDetails()
    }
    
    func showDetails() {
        
        let companyViewController = CompanyViewController.initFromStoryboard(named: "Main")
        companyViewController.company = self.company
        companyViewController.delegate = self
        companyViewController.baseDelegate = self
        self.navigationController.pushViewController(companyViewController, animated: true)
    }
}

// MARK: - View Controller delegate

extension CompanyCoordinator: ECViewControllerDelegate {
    
    func viewControllerDidExit(_ viewController: UIViewController) {
        
        if viewController is CompanyViewController {
            self.coordinatorDelegate?.coordinatorDidExit(self)
        }
    }
}

// MARK: - Company View Controller delegate

extension CompanyCoordinator: CompanyViewControllerDelegate {
    
    func didTapToggleFavorite(on viewController: CompanyViewController) {
        
        if self.company.isFavorite {
            viewController.setFavoriteOff()
            CompanyAPI.unfavorite(company: self.company, completion: { _, error, _ in
                if error != nil {
                    viewController.setFavoriteOn()
                }
            })
        } else {
            viewController.setFavoriteOn()
            CompanyAPI.favorite(company: self.company, completion: { _, error, _ in
                if error != nil {
                    viewController.setFavoriteOff()
                }
            })
        }
    }
}
