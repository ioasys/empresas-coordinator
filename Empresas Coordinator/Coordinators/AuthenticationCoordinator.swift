//
//  AuthenticationCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol AuthenticationCoordinatorDelegate: CoordinatorDelegate {
    func didAuthenticate(user: User, on coordinator: AuthenticationCoordinator)
}

class AuthenticationCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var delegate: AuthenticationCoordinatorDelegate?
    var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController, delegate: AuthenticationCoordinatorDelegate) {
        self.navigationController = navigationController
        self.delegate = delegate
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        self.showLogin()
    }
    
    func showLogin() {
        
        let loginViewController = LoginViewController.initFromStoryboard(named: "Main")
        loginViewController.delegate = self
        self.navigationController.setViewControllers([loginViewController], animated: false)
    }
}

// MARK: - Login View Controller delegate

extension AuthenticationCoordinator: LoginViewControllerDelegate {
    
    func didFill(email: String, password: String, on viewController: LoginViewController) {
        
        let hud = MBProgressHUD.showAdded(to: self.navigationController.view, animated: true)
        CompanyAPI.login(withEmail: email, password: password) { response, error, cache in
            hud.hide(animated: true)
            
            if let response = response {
                self.delegate?.didAuthenticate(user: response, on: self)
            }
        }
    }
}
