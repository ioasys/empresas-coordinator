//
//  AppCoordinator.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit
import MBProgressHUD

class AppCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        
        if UserDefaults.standard.object(forKey: APIRequest.Constants.authenticationHeadersDefaultsKey) == nil {
            self.showLogin()
            return
        }
        
        CompanyAPI.userProfile(completion: { response, error, cache in
            if let response = response {
                self.showTabBar(withUser: response)
            }
        })
    }
    
    func handle(url: URL) {
        guard let host = url.host, let companyID = Int(host) else { return }
        
        let hud = MBProgressHUD.showAdded(to: self.navigationController.view, animated: true)
        CompanyAPI.company(withID: companyID) { response, error, cache in
            hud.hide(animated: true)
            
            if let response = response {
                self.show(company: response)
            }
        }
    }
    
    func showLogin() {
     
        let coordinator = AuthenticationCoordinator(navigationController: self.navigationController, delegate: self)
        coordinator.start()
        
        self.childCoordinators.append(coordinator)
    }
    
    func showTabBar(withUser user: User) {
        
        let coordinator = TabBarCoordinator(user: user, navigationController: self.navigationController, delegate: self)
        coordinator.start()
        
        self.childCoordinators.append(coordinator)
    }
    
    func show(company: Company) {
        
        if let coordinator = self.childCoordinators[0] as? TabBarCoordinator {
            coordinator.show(company: company)
        }
    }
}

// MARK: - Authentication Coordinator delegate

extension AppCoordinator: AuthenticationCoordinatorDelegate {
    
    func didAuthenticate(user: User, on coordinator: AuthenticationCoordinator) {
        self.childCoordinators.removeLast()
        self.showTabBar(withUser: user)
    }
}

// MARK: - Tab Bar Coordinator delegate

extension AppCoordinator: TabBarCoordinatorDelegate {
    
    func didAskForLogout(on coordinator: TabBarCoordinator) {
        
        APICacheManager.shared.clearCache()
        UserDefaults.standard.set(nil, forKey: APIRequest.Constants.authenticationHeadersDefaultsKey)
        self.childCoordinators = []
        self.showLogin()
    }
}
