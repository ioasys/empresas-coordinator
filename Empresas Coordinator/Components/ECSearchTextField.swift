//
//  ECSearchTextField.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class ECSearchTextField: UITextField {
    
    private let height = 28 as CGFloat
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }

    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: self.height))
        
        self.placeholder = "Pesquisar"
        self.font = UIFont.systemFont(ofSize: 14)
        self.clearButtonMode = .always
        self.returnKeyType = .go
        self.tintColor = .black
        self.backgroundColor = .white
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(self.heightAnchor.constraint(equalToConstant: self.height))
        
        self.layer.cornerRadius = 5
        
        let viewBorder = 8 as CGFloat
        
        let image = #imageLiteral(resourceName: "searchSmall")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: viewBorder, y: (self.height - image.size.height) / 2, width: image.size.width, height: image.size.height)
        
        let leftViewFrame = CGRect(x: 0, y: 0, width: image.size.width + (viewBorder * 2), height: self.height)
        let leftView = UIView(frame: leftViewFrame)
        leftView.addSubview(imageView)
        
        self.leftView = leftView
        self.leftViewMode = .always
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
