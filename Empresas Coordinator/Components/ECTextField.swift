//
//  ECTextField.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

@IBDesignable
class ECTextField: UITextField {
    
    private static let height = 40 as CGFloat
    private var bottomLine: UIView?
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            if let leftImage = self.leftImage {
                let viewBorder = 5 as CGFloat
                
                let imageView = UIImageView(image: leftImage)
                imageView.frame = CGRect(x: viewBorder,
                                         y: (ECTextField.height - leftImage.size.height) / 2,
                                         width: leftImage.size.width,
                                         height: leftImage.size.height)
                
                let leftViewFrame = CGRect(x: 0, y: 0, width: leftImage.size.width + (viewBorder * 2), height: ECTextField.height)
                let leftView = UIView(frame: leftViewFrame)
                leftView.addSubview(imageView)
                
                self.leftView = leftView
                self.leftViewMode = .always
            } else {
                self.leftView = nil
                self.leftViewMode = .never
            }
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIViewNoIntrinsicMetric, height: ECTextField.height)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    func commonInit() {
        guard self.bottomLine == nil else { return }
        
        let bottomLine = UIView()
        bottomLine.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2156862745, blue: 0.262745098, alpha: 1)
        bottomLine.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(bottomLine)
        
        let leftConstraint = self.leftAnchor.constraint(equalTo: bottomLine.leftAnchor)
        let rightConstraint = self.rightAnchor.constraint(equalTo: bottomLine.rightAnchor)
        let bottomConstraint = self.bottomAnchor.constraint(equalTo: bottomLine.bottomAnchor)
        let heightConstraint = bottomLine.heightAnchor.constraint(equalToConstant: 1)
        self.addConstraints([leftConstraint, rightConstraint, bottomConstraint])
        bottomLine.addConstraint(heightConstraint)
    }
}
