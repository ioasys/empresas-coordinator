//
//  CompanyViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol CompanyViewControllerDelegate: class {
    func didTapToggleFavorite(on viewController: CompanyViewController)
}

class CompanyViewController: BaseViewController {

    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    weak var delegate: CompanyViewControllerDelegate?
    var company: Company!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.company == nil {
            return
        }

        self.title = self.company.name
        
        var companyName = self.company.name
        self.initialsLabel.text = "\(companyName.removeFirst())\(companyName.removeFirst())".uppercased()
        self.descriptionLabel.text = self.company.description
        
        if self.company.isFavorite {
            self.setFavoriteOn()
        } else {
            self.setFavoriteOff()
        }
    }
    
    func setFavoriteOn() {
        
        let barButton = UIBarButtonItem.barButton(withImage: #imageLiteral(resourceName: "heart-on"), target: self, action: #selector(toggleFavorite))
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    func setFavoriteOff() {
        
        let barButton = UIBarButtonItem.barButton(withImage: #imageLiteral(resourceName: "heart-off"), target: self, action: #selector(toggleFavorite))
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }

    @objc func toggleFavorite() {
        self.delegate?.didTapToggleFavorite(on: self)
    }
}
