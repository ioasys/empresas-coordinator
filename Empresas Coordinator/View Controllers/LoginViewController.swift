//
//  LoginViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func didFill(email: String, password: String, on viewController: LoginViewController)
}

class LoginViewController: ECViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    weak var delegate: LoginViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func validate() -> Bool {
        
        var errorMessage: String?
        var textFieldToFocus: UITextField?
        
        if !self.emailTextField.hasText {
            errorMessage = NSLocalizedString("Insira o e-mail", comment: "")
            textFieldToFocus = self.emailTextField
        } else if !self.passwordTextField.hasText {
            errorMessage = NSLocalizedString("Insira a senha", comment: "")
            textFieldToFocus = passwordTextField
        }
        
        if let errorMessage = errorMessage {
            let alertController = UIAlertController(title: NSLocalizedString("Errinho", comment: ""), message: errorMessage, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { _ in
                textFieldToFocus?.becomeFirstResponder()
            }))
            self.present(alertController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func go(_ sender: Any) {
        guard self.validate() else { return }
        self.delegate?.didFill(email: self.emailTextField.text!, password: self.passwordTextField.text!, on: self)
    }
}

// MARK: - Text Field delegate

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.go(textField)
        }
        
        return true
    }
}
