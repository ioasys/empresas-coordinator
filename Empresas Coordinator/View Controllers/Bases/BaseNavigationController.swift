//
//  BaseNavigationController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
