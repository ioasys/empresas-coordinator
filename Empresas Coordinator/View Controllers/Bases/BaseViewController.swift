//
//  BaseViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class BaseViewController: ECViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        if (self.title ?? " ") == " " && self.navigationItem.titleView == nil {
            let image =  #imageLiteral(resourceName: "logoNav")
            let titleView = UIView(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
            titleView.clipsToBounds = true
            
            let titleImageView = UIImageView(image: image)
            titleImageView.contentMode = .scaleAspectFit
            titleView.addSubview(titleImageView)
            
            self.navigationItem.titleView = titleView
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if let navigationController = self.navigationController, navigationController.viewControllers.count > 1 {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.barButton(withImage: #imageLiteral(resourceName: "backChevron"), target: self, action: #selector(goBack(_:)))
            
            weak var weakNav = self.navigationController
            self.navigationController?.interactivePopGestureRecognizer?.delegate = weakNav as? UIGestureRecognizerDelegate
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension UIBarButtonItem {
    
    static func barButton(withImage image: UIImage, target: Any?, action: Selector?) -> UIBarButtonItem {
        
        let button = UIButton(type: .system)
        button.isUserInteractionEnabled = true
        button.setBackgroundImage(image, for: .normal)
        button.frame = CGRect(x: 0, y: 6, width: image.size.width, height: image.size.height)
        
        if let target = target, let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        
        return UIBarButtonItem(customView: button)
    }
}

