//
//  HomeViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol HomeViewControllerDelegate: class {
    func didSelect(company: Company, on viewController: HomeViewController)
}

class HomeViewController: BaseViewController {
    
    @IBOutlet var searchResultsContainer: UIView!
    
    weak var delegate: HomeViewControllerDelegate?
    private var companyListViewController: CompanyListViewController!
    private var searchRequest: CompanyAPI?
    private var previousTitleView: UIView?
    private var previousRightBarButtonItem: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.barButton(withImage: #imageLiteral(resourceName: "icSearchCopy"), target: self, action: #selector(openSearch))
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "companyList" {
            self.companyListViewController = segue.destination as! CompanyListViewController
            self.companyListViewController.delegate = self
        }
    }
    
    @objc func openSearch() {
        
        self.previousTitleView = self.navigationItem.titleView
        self.previousRightBarButtonItem = self.navigationItem.rightBarButtonItem
        
        let textField = ECSearchTextField(frame: .zero)
        textField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        textField.delegate = self
        UIView.animate(withDuration: 0.25, animations: {
            self.navigationItem.titleView = textField
            self.navigationItem.titleView?.layoutIfNeeded()
        }, completion: { _ in
            textField.becomeFirstResponder()
        })
        
        let barButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelSearch))
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    @objc func textFieldChanged(_ textField: UITextField) {
        
        if !textField.hasText {
            UIView.animate(withDuration: 0.25, animations: {
                self.searchResultsContainer.alpha = 0
            }, completion: { _ in
                self.companyListViewController.companies = []
            })
            return
        }
        
        UIView.animate(withDuration: 0.25) {
            self.searchResultsContainer.alpha = 1
        }
        
        self.searchRequest?.task?.cancel()
        self.searchRequest = CompanyAPI.searchCompanies(query: textField.text!) { response, error, cache in
            
            if let response = response {
                self.companyListViewController.companies = response
            }
        }
    }
    
    @objc func cancelSearch() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.searchResultsContainer.alpha = 0
        }, completion: { _ in
            self.companyListViewController.companies = []
        })
        
        self.previousTitleView?.resignFirstResponder()
        UIView.animate(withDuration: 0.25) {
            self.navigationItem.titleView = self.previousTitleView
            self.navigationItem.titleView?.layoutIfNeeded()
        }
        
        if let barButton = self.previousRightBarButtonItem {
            self.navigationItem.setRightBarButton(barButton, animated: true)
        }
        
        self.previousTitleView = nil
        self.previousRightBarButtonItem = nil
    }
}

// MARK: - Text Field delegate

extension HomeViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Company List View Controller delegate

extension HomeViewController: CompanyListViewControllerDelegate {
    
    func didSelect(company: Company, on viewController: CompanyListViewController) {
        self.delegate?.didSelect(company: company, on: self)
    }
}
