//
//  ProfileViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol ProfileViewControllerDelegate: class {
    func didTapLogout(on viewController: ProfileViewController)
}

class ProfileViewController: BaseViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    weak var delegate: ProfileViewControllerDelegate?
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameLabel.text = self.user.name
        self.emailLabel.text = self.user.email
        self.locationLabel.text = "\(self.user.city), \(self.user.country)"
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        self.delegate?.didTapLogout(on: self)
    }
}
