//
//  CompanyListViewController.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

protocol CompanyListViewControllerDelegate: class {
    func didSelect(company: Company, on viewController: CompanyListViewController)
}

class CompanyListViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    
    weak var delegate: CompanyListViewControllerDelegate?
    var companies: [Company] = [] {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // https://gist.github.com/smileyborg/ec4812c146f575cd006d98d681108ba8
        if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
            if let coordinator = self.transitionCoordinator {
                coordinator.animate(alongsideTransition: { _ in
                    self.tableView.deselectRow(at: selectedIndexPath, animated: true)
                }, completion: { context in
                    if context.isCancelled {
                        self.tableView.selectRow(at: selectedIndexPath, animated: false, scrollPosition: .none)
                    }
                })
            } else {
                self.tableView.deselectRow(at: selectedIndexPath, animated: animated)
            }
        }
    }
}

// MARK: - Table View data source / delegate

extension CompanyListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyCell
        cell.company = self.companies[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelect(company: self.companies[indexPath.row], on: self)
    }
}
