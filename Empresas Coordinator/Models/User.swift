//
//  User.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import Foundation

class User: Mappable {
    
    var id: Int
    var name: String
    var email: String
    var city: String
    var country: String
    
    required init(mapper: Mapper) {
        
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
    }
}
