//
//  Company.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import Foundation

class Company: Mappable {
    
    static var favoriteCompanies: [Company] = []

    var id: Int
    var name: String
    var type: String
    var country: String
    var description: String
    var isFavorite: Bool {
        return Company.favoriteCompanies.contains(self)
    }
    
    required init(mapper: Mapper) {
        
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("enterprise_name")
        self.type = mapper.keyPath("enterprise_type.enterprise_type_name")
        self.country = mapper.keyPath("country")
        self.description = mapper.keyPath("description")
    }
}

extension Company: Equatable {
    
    static func ==(lhs: Company, rhs: Company) -> Bool {
        return lhs.id == rhs.id
    }
}
