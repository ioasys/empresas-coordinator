//
//  CompanyCell.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {

    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    var company: Company! {
        didSet {
            var companyName = self.company.name
            self.initialsLabel.text = "\(companyName.removeFirst())\(companyName.removeFirst())".uppercased()
            self.nameLabel.text = self.company.name
            self.typeLabel.text = self.company.type
            self.countryLabel.text = self.company.country
        }
    }
}
