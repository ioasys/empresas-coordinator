//
//  CompanyAPI.swift
//  Empresas Coordinator
//
//  Created by Jota Melo on 1/21/18.
//  Copyright © 2018 ioasys. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]
typealias JSONArray = [JSONDictionary]

class CompanyAPI: APIRequest {
    
    @discardableResult
    static func login(withEmail email: String, password: String, completion: ResponseBlock<User>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { response, error, cache in
            
            if let response = response as? JSONDictionary, let userDictionary = response["investor"] as? JSONDictionary {
                let user = User(dictionary: userDictionary)
                completion?(user, nil, cache)
                self.userProfile(completion: nil)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func userProfile(completion: ResponseBlock<User>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "investors", parameters: nil, urlParameters: nil, cacheOption: .cacheOnly) { response, error, cache in
            
            if let response = response as? JSONDictionary, let userDictionary = response["investor"] as? JSONDictionary {
                let user = User(dictionary: userDictionary)
                completion?(user, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.suppressErrorAlert = true
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func searchCompanies(query: String, completion: ResponseBlock<[Company]>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "enterprises", parameters: nil, urlParameters: ["name": query], cacheOption: .both) { response, error, cache in
            
            if let response = response as? JSONDictionary, let companiesDictionaries = response["enterprises"] as? JSONArray {
                let companies = companiesDictionaries.map { Company(dictionary: $0) }
                completion?(companies, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func company(withID id: Int, completion: ResponseBlock<Company>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "enterprises/\(id)", parameters: nil, urlParameters: nil, cacheOption: .cacheOnly) { response, error, cache in
            
            if let response = response as? JSONDictionary, let companyDictionary = response["enterprise"] as? JSONDictionary {
                let company = Company(dictionary: companyDictionary)
                completion?(company, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func favorites(completion: ResponseBlock<[Company]>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "enterprises", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { response, error, cache in
            
            if error == nil {
                completion?(Company.favoriteCompanies, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func favorite(company: Company, completion: ResponseBlock<Any>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "enterprises/\(company.id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { response, error, cache in
            
            if error == nil {
                Company.favoriteCompanies.append(company)
                completion?(nil, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func unfavorite(company: Company, completion: ResponseBlock<Any>?) -> CompanyAPI {
        
        let request = CompanyAPI(method: .get, path: "enterprises/\(company.id)", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { response, error, cache in
            
            if error == nil {
                if let index = Company.favoriteCompanies.index(of: company) {
                    Company.favoriteCompanies.remove(at: index)
                }
                
                completion?(nil, nil, cache)
            } else {
                completion?(nil, error, cache)
            }
        }
        request.makeRequest()
        
        return request
    }
}
